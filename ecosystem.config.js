module.exports = {
  apps: [{
    name: "osas-reg-form",
    script: "./node_modules/nuxt-start/bin/nuxt-start.js",
    env: {
      "HOST": "0.0.0.0",
      "PORT": 3200,
      "NODE_ENV": "production",
    }
  }]
}
