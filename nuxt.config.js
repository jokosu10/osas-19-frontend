module.exports = {
  /*
   ** Headers of the page
   */
  head: {
    title: 'Pendaftaran openSUSE.Asia Summit 2019',
    meta: [{
        charset: 'utf-8'
      },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1'
      },
      {
        hid: 'description',
        name: 'description',
        content: 'Pendaftaran openSUSE.Asia Summit 2019'
      }
    ],
    link: [{
        rel: 'icon',
        type: 'image/x-icon',
        href: '/images/favicon.png'
      },
      {
        rel: 'stylesheet',
        href: '/css/style.css'
      },
      {
        rel: 'stylesheet',
        href: '/vendor/nouislider/nouislider.min.css'
      },
      {
        rel: 'stylesheet',
        href: '/fonts/material-icon/css/material-design-iconic-font.css'
      }
    ],
  },

  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/toast',
    'vue-sweetalert2/nuxt'
  ],

  axios: {
    // proxyHeaders: false
  },
  toast: {
    position: 'top-center',
    register: [ // Register custom toasts
      {
        name: 'my_qrcode',
        message: 'Success registration, please scan QR-Code for payment',
        options: {
          type: 'success'
        }
      }
    ]
  },
  /*
   ** Customize the progress bar color
   */
  loading: {
    color: '#3B8070'
  },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** Run ESLint on save
     */
    extend(config, {
      isDev,
      isClient
    }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  },
  server: {
    port: 3200, // default: 3000
    host: '127.0.0.1', // default: localhost
  },
}
